var profileDisabled = true;
var passwordDisabled = true;

function signup (response) {
	var data = response;
	if (data.status == 'success') {
		alert('Pendaftaran Akun Berhasil');
		location.href = '/supplier/';
	} else {
		if (data.info == '401') {
			$('#signup-alert strong').text('Kata Sandi Salah');
		}
		var username = $('input[name=username]');
		var errAlert = $('#signup-alert');
		var clsAlert = errAlert.attr('class').split(' ');
		clsAlert[clsAlert.length - 1] = 'show';
		errAlert.attr('class', clsAlert.join(' '));
		username.parent().attr('class', 'form-group has-error');
		username.focus();
	}
}

function checkSignUp (ev) {
	var password = $('#password');
	var repassword = $('#repassword');
	var mobile = $('#mobile');
	var pattern = new RegExp('\\D');
	var alert = $('#signup-alert');
	if (password.val() != repassword.val()) {
		ev.preventDefault();
		alert.children().text('Password Tidak Sama !');
		var clsAlert = alert.attr('class').split(' ');
		clsAlert[clsAlert.length - 1] = 'show';
		alert.attr('class', clsAlert.join(' '));
		repassword.parent().attr('class', 'form-group has-error');
		repassword.val('');
		repassword.focus();
	} else if (pattern.test(mobile.val())){
		ev.preventDefault();
		alert.children().text('No Telepon Anda Salah !');
		var clsAlert = alert.attr('class').split(' ');
		clsAlert[clsAlert.length - 1] = 'show';
		alert.attr('class', clsAlert.join(' '));
		mobile.parent().attr('class', 'form-group has-error');
		mobile.focus();
	}
}

function signin (response) {
	var data = response;
	if (data.status == 'success') {
		location.href = '/supplier/main/';
	} else {
		if (data.info == '401') {
			$('#signin-alert strong').html('Kata Sandi Salah');
		} else if (data.info == '402') {
			$('#signin-alert strong').html('Akun Tidak Terdaftar');
		}
		var alert = $('#signin-alert');
		var clsAlert = alert.attr('class').split(' ');
		clsAlert[clsAlert.length - 1] = 'show';
		alert.attr('class', clsAlert.join(' '));
	}
}

function setResolution (width) {
	var leftMenu = $('#left-menu');
	var clientWidth = (width ? width : window.innerWidth);
	if (clientWidth > 992) {
		leftMenu.attr('class', 'left-menu left-large');
		leftMenu.css('display', 'block');
	} else if (clientWidth >= 768 && clientWidth <= 992) {
		leftMenu.attr('class', 'left-menu left-medium');
		leftMenu.css('display', 'block');
	} else {
		leftMenu.attr('class', 'left-menu');
		leftMenu.css('display', 'none');
	}
}

function setLeftSelection (index, id) {
	var leftMenu = $('#'+id+'', parent.document);
	$('#left-menu li', parent.document).each(function () {
		$(this).attr('class', '');
	});
	switch (index) {
		case 1:
			leftMenu.addClass('active');
			break;
		case 2:
			leftMenu.addClass('active');
			break;
	}
}

function checkError (objInput) {
	objInput.parentElement.className += ' has-error';
}

function updateProfile () {
	var btnEdit = $('#btn-edit-profile');
	var btnCancel = $('#btn-cancel-profile');
	var btnSave = $('#btn-save-profile');
	if (profileDisabled) {
		btnEdit.addClass('hidden');
		btnSave.removeClass('hidden');
		btnCancel.removeClass('hidden');
		$('.profile').each(function () {
			$(this).prop('readonly', false);
		});
	} else {
		btnEdit.removeClass('hidden');
		btnSave.addClass('hidden');
		btnCancel.addClass('hidden');
		$('.profile').each(function () {
			$(this).prop('readonly', true);
		});
	}
	profileDisabled = !profileDisabled;
}

function updatePassword (data) {
	var btnEditPass = $('#btnEditPass');
	var btnCancelPass = $('#btnCancalPass');
	var btnSavePass = $('#btnSavePass');
	if (passwordDisabled) {
		btnEditPass.addClass('hidden');
		btnCancelPass.removeClass('hidden');
		btnSavePass.removeClass('hidden')
		$('.password').each(function () {
			$(this).prop('readonly', false);
		});
	} else {
		btnEditPass.removeClass('hidden');
		btnCancelPass.addClass('hidden');
		btnSavePass.addClass('hidden')
		$('.password').each(function () {
			$(this).val('');
			$(this).prop('readonly', true);
		});
	}
	if (data) {
		if (data.status == 'success') {
			alert('Pembaharuan Password Berhasil');
		} else {
			alert('Pembaharuan Password Gagal');
		}
	}
	passwordDisabled = !passwordDisabled;
}

function checkPass () {
	var oldPass = $('#old-password');
	var newPass = $('#new-password');
	var rePass = $('#re-password');
	if (newPass.val() != rePass.val()) {
		newPass.parent().addClass("has-error");
		rePass.parent().addClass("has-error");
		alert("Password Tidak Sama");
		return false;
	}
}

function wrongPass () {
	var oldPass = $('#old-password');
	oldPass.parent().addClass('has-error');
	oldPass.focus();
	alert("Password Lama Anda Salah");
}