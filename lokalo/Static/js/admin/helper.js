/**
 * Created by Felix on 3/22/2016.
 */

function signin (response) {
	var data = response;
	if (data.status == 'success') {
		location.href = '/adminPanel/dashboard/';
	} else {
        if (data.info == '401') {
            $('#signin-alert strong').text('Nama Akun / Kata Sandi Salah');
        } else if (data.info == '402') {
            $('#signin-alert strong').text('Akun Tidak Terdaftar');
        }
		var alert = $('#signin-alert');
		var clsAlert = alert.attr('class').split(' ');
		clsAlert[clsAlert.length - 1] = 'show';
		alert.attr('class', clsAlert.join(' '));
	}
}

function checkError (objInput) {
	objInput.parentElement.className += ' has-error';
}

function setLeftActive (index) {
    switch (index) {
        case '1':
            $('#admin-leftmenu').addClass('submenu-active');
            break;
        case '2':
            $('#supplier-leftmenu').addClass('submenu-active');
            break;
        case '3':
            $('#category-leftmenu').addClass('submenu-active');
            break;
        case '4':
            $('#shipping-leftmenu').addClass('submenu-active');
            break;
        case '5':
            $('#email-leftmenu').addClass('submenu-active');
            break;
    }
}

function initEditor () {
    tinymce.remove();
    tinymce.init({
        selector: 'textarea#description',
        theme: 'modern',
        height: 300,
        element_format: 'html',
        toolbar: 'insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontsizeselect | fontselect',
        resize: false,
        statusbar: false,
        custom_unde_redo_levels: 50
    });
}

function setCookie(key, value) {
    document.cookie = ' '+key+'='+value+' ';
}

function getCookie(key) {
    var i, curCookie;
    var value = "";
    var keyCookie = key + "=";
    var listCookie = document.cookie.split(';')
    for (i=0; i<listCookie.length; i++) {
        curCookie = listCookie[i];
        if (curCookie.charAt(0) == ' ') {
            curCookie = curCookie.substring(1);
        }
        if (curCookie.indexOf(keyCookie) == 0) {
            value = curCookie.substring(keyCookie.length, curCookie.length);
        }
    }
    return value;
}

function clearDiscount () {
    $('#discount').val('');
    $('#newPrice').val('');
}

function clearContCategory () {
    $('#add-category').html('');
    $('#update-category').html('');
    $('#list-category').html('');
}

function clearContProduct () {
    $('#add-product').html('');
    $('#list-product').html('');
    $('#edit-product').html('');
}

function checkEmpty (obj) {
    obj.parentElement.className += ' has-error';
}

function disabledBtntop (page) {
    /* leftmenu number
    1 = dashboard
    2 = category
    3 = product
    */
    switch (page) {
        case '2':
            $('#btn-addCategory').prop('disabled', true);
            $('#btn-listCategory').prop('disabled', true);
            break;
        case '3':
            $('#btn-addProduct').prop('disabled', true);
            $('#btn-listProduct').prop('disabled', true);
            break;
    }
}

function enabledBtnTop (page) {
    /* leftmenu number
    1 = dashboard
    2 = category
    3 = product
    */
    switch (page) {
        case '2':
            $('#btn-addCategory').prop('disabled', false);
            $('#btn-listCategory').prop('disabled', false);
            break;
        case '3':
            $('#btn-addProduct').prop('disabled', false);
            $('#btn-listProduct').prop('disabled', false);
            break;
    }
}

function checkError (objInput) {
	objInput.parentElement.className += ' has-error';
}