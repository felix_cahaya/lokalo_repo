/**
 * Created by Felix on 3/12/2016.
 */
// page supplier
function validateAddSup () {
    var email = $('#email');
    var password = $('#password');
    var repass = $('#re-password');
    if ( password.val() != repass.val() ){
        repass.parent().addClass('has-error');
        repass.val('');
        repass.focus();
        alert('Password doesn\'t match');
        return false;
    }
}

function addSupResponse(data) {
    if (data.status == 'success') {
        alert('New Supplier Added');
        window.location = '/adminPanel/supplier/';
    } else {
        if (data.info == '401') {
            var username = $('#username');
            username.parent().addClass('has-error');
            username.val('')
            username.focus();
            alert('Account Already Exist');
        }
    }
}

function deleteSupCnfrm (name) {
    var cnfrm = confirm("Remove Supplier "+name+" ?");
    if (!cnfrm) {
        return false;
    }
    return true;
}

function deleteSupResponse (data) {
    if (data.status == 'success') {
        window.location = '/adminPanel/supplier/';
    } else {
        alert('Delete Failed');
    }
}
// end page supplier

// page category
function saveCategory(ev) {
    ev.preventDefault();
    var dataPost = {};
    var categoryName = $('input[name=categoryName]');
    var description = $('textarea[name=description]');
    var publish = $('input[name=publish]');
    var objRequired = [categoryName];
    var addSubCat = getCookie('addSubCat');
    var levelCategory = getCookie('levelCategory');
    var parentIdCat = getCookie('parentIdCat');
    dataPost = {
        'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val(),
        'add':'true',
        'categoryName':categoryName.val().toUpperCase(),
        'description':description.val(),
        'publish':publish.is(':checked'),
        'level':levelCategory,
        'addSubCategory': addSubCat
    };
    if (addSubCat == '1') {
        dataPost.addSubCategory = addSubCat;
        dataPost.parentId = parentIdCat;
        dataPost.level = levelCategory;
    }
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/category/',
        data: dataPost,
        success: function (response) {
            if (response == "success") {
                setCookie('addSubCat', '0');
                alert('Category '+categoryName.val()+' Saved');
                rdrAddCategory('0', '1');
            } else {
                alert("Failed Add Category");
            }
        }
    });
}

function rdrAddCategory (subCategory, level, parentId) {
    setCookie('addSubCat', subCategory);
    setCookie('levelCategory', level);
    setCookie('parentIdCat', parentId);
    var addCatContainer = $('#add-category');
    disabledBtntop('2');
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/adminPanel/category?add=y',
        beforeSend: function () {
            clearContCategory();
            addCatContainer.html('<div class="col-xs-12 col-sm-10 col-md-8"><div class="box-product centerText"><i class="fa fa-refresh fa-spin fa-4x"></i></div></div>');
        },
        success: function (response) {
            addCatContainer.html(response);
        },
        complete: function () {
            enabledBtnTop('2');
        }
    });
}

function rdrEditCategory (id, level) {
    var editCatContainer = $('#update-category');
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/adminPanel/category?edit=y&id='+id+'&level='+level,
        beforeSend: function () {
            clearContCategory();
            editCatContainer.html('<div class="col-xs-12 col-sm-10"><div class="box-product centerText"><i class="fa fa-refresh fa-spin fa-4x"></i></div></div>');
        },
        success: function (response) {
            editCatContainer.html(response);
        }
    });
}

function updateCategory (ev) {
    ev.preventDefault();
    var ID = $('input[name=updateID]').val();
    var level = $('input[name=level]').val();
    var name = $('input[name=categoryNameUpdate]').val().toUpperCase();
    var dataPost = {
        'csrfmiddlewaretoken' : $('input[name=csrfmiddlewaretoken]').val(),
        'update' : 'true',
        'name' : name,
        'description' : $('textarea[name=descriptionUpdate]').val(),
        'publish' : $('input[name=publishUpdate]').is(':checked'),
        'id' : ID,
        'level' : level
    };
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/category/',
        data: dataPost,
        success: function (response) {
            if (response == "success") {
                alert('Category '+name+' Updated');
                clearContCategory();
                rdrAddCategory();
            } else {
                alert('Update Category Failed');
            }
        }
    });
}

function removeCategory (id, level, name) {
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/category/',
        data: {'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val(), 'remove':'true', 'id':id, 'level':level},
        success: function (response) {
            if (response == "success") {
                alert("Category "+name+" Removed");
                rdrListCategory('listCategory', 1);
            }
        }
    });
}

function rdrListCategory (type, level, id) {
    setCookie('levelCategory', level);
    var listCatContainer = $('#list-category');
    var levelCategory = level;
    var query = '';
    if (type == 'listCategory') {
        query = 'listCategory=true';
    } else {
        query = 'listSubCategory=true&id='+id+'&level='+level+'';
    }
    disabledBtntop('2');
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/adminPanel/category?'+query+'',
        beforeSend: function () {
            clearContCategory();
            listCatContainer.html('<div class="col-xs-12 col-sm-10 col-md-8"><div class="box-product centerText"><i class="fa fa-refresh fa-spin fa-4x"></i></div></div>');
        },
        success: function (response) {
            listCatContainer.html(response);
        },
        complete: function () {
            enabledBtnTop('2');
        }
    });
}
// end page category

// page product
function addMoreCategory () {
    var container = $('#more-category');
    var template = $('#category').html();
    container.append(['<select class="form-control slctCategory">', template ,'</select>'].join(''));
    $('#btn-remove-category').removeClass('hide');
}

function removeMoreCategory () {
    var container = $('#more-catgeory');
    var listSlctCategory = $('.slctCategory');
    listSlctCategory.last()[0].remove();
    if ( (listSlctCategory.length - 1) == 1) {
        $('#btn-remove-category').addClass('hide');
    }
}

function addMoreStock () {
    var obj = $('table.table tbody');
    var stockInputTemp = obj.children().first().html();
    stockInputTemp = stockInputTemp.replace('id="stock" ', '');
    obj.append(['<tr class="stock-section">',stockInputTemp,'</tr>'].join(''));
    var btnRemove = $('#btn-stock button.btn-danger');
    if (btnRemove.hasClass('hide')){
        btnRemove.removeClass('hide');
    }
}

function removeMoreStock () {
    var obj = $('table.table tbody');
    obj.children().last().remove();
    if (obj.children().length == 1) {
        $('#btn-stock button.btn-danger').addClass('hide');
    }
}

function addDiscount () {
    var priceBox = $('#price-box');
    priceBox.removeClass('hide');
    $('#btn-hide-discount').removeClass('hide');
}

function hideDiscount () {
    var priceBox = $('#price-box');
    priceBox.addClass('hide');
    $('#btn-hide-discount').addClass('hide');
}

function countDiscount () {
    var objPrice = $('#price');
    var objDisc = $('#discount');
    var objNewPrice = $('#newPrice');
    if (objPrice.val() != '' && objDisc.val() != '') {
        var newPrice = parseInt(objPrice.val()) - ((parseInt(objPrice.val()) * parseInt(objDisc.val())) / (100));
        objNewPrice.val(newPrice);
    } else if (objPrice.val() != '' && objDisc.val() == '') {
        objNewPrice.val('');
    } else if (objPrice.val() == '') {
        objPrice.focus();
        objDisc.val('');
        alert('Please Fill Price Column First');
    }
}

function validateProduct (e) {
    e.preventDefault();
    var cat = $('#category');
    if (cat.val() == 0){
        cat.parent()[0].className += ' has-error';
        cat.focus();
        alert("Please Select Category");
    } else {
        var data = prepareDataProduct();
        data.csrfmiddlewaretoken = $('input[name=csrfmiddlewaretoken]').val();
        data.save = 'y';
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/adminPanel/product/',
            data: data,
            success: function (response) {
                if (response == "success") {
                    alert("Product With Code "+data.productName+" Successfully Added");
                    rdrListProduct("1");
                } else {
                    alert(response);
                }
            }
        });
    }
}

function prepareDataProduct () {
    var data = {};
    var i = 0;
    var category = [];
    $('.slctCategory').each(function () {
        if ($(this).val() != '0') {
            category.push($(this).val());
        }
    });
    data.category = category.join('|');
    data.productCode = $('#product-code').val();
    data.productName = $('#product-name').val();
    data.brand = $('#brand').val();
    data.color = $('#color').val();
    var ss = []; // stock | size
    $('.stock').each(function () {
        ss.push($(this).val());
    });
    $('.size').each(function () {
        if ($(this).val() == '') {
            ss[i] += '~-';
        } else {
            ss[i] += '~'+$(this).val()+'';
        }
        i++;
    });
    data.ss = ss.join('|');
    data.lastUpdate = $('#dateStock').val();
    data.price = $('#price').val();
    var discount = $('#discount');
    if (discount.val() != '' && discount.val() != '0.0') {
        data.discount = discount.val();
        data.newPrice = $('#newPrice').val();
        data.discExp = $('#dateDiscount').val();
    }
    data.weight = $('#weight').val();
    data.weightName = $('input[name=weightUnit]:checked').val();
    data.description = tinymce.activeEditor.getContent();
    return data;
}

function rdrAddProduct () {
    clearContProduct();
    var addProductCont = $('#add-product');
    addProductCont.html('');
    var template;
    disabledBtntop('3');
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/adminPanel/product?add-product=y',
        beforeSend: function () {
            addProductCont.html('<div class="col-xs-12 col-sm-10"><div class="box-product centerText"><i class="fa fa-refresh fa-spin fa-4x"></i></div></div>');
        },
        success: function (response) {
            template = response;
        },
        complete: function () {
            addProductCont.html(template);
            setupDate();
            initEditor();
            enabledBtnTop('3');
        }
    });
}

function rdrListProduct (page) {
    clearContProduct();
    var currentPage = getCookie('page');
    if (currentPage == "" || currentPage != page) {
        setCookie('page', page.toString());
    }
    var listProductCont = $('#list-product');
    var url = '/adminPanel/product?list-product=y&page='+page.toString();
    var template;
    disabledBtntop('3');
    $.ajax({
        type: 'GET',
        cache: false,
        url: url,
        beforeSend: function () {
            listProductCont.html('<div class="col-xs-12 col-sm-10"><div class="box-product centerText"><i class="fa fa-refresh fa-spin fa-4x"></i></div></div>');
        },
        success: function (response) {
            template = response
        },
        complete: function () {
            listProductCont.html(template);
            enabledBtnTop('3');
        }
    });
}

function setMainImage(product_id, name) {
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/product/',
        data: {'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val(), 'updateImg':'y', 'id':product_id, 'name':name},
        success: function (response) {
            if (response == "success") {
                rdrListProduct(getCookie('page'));
            }
        }
    });
}

function rdrEditProduct(id) {
    clearContProduct();
    setCookie('rdrEditProduct', id);
    var editContainer = $('#edit-product');
    var template;
    disabledBtntop('3');
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/adminPanel/product?edit-page=y&id='+id.toString(),
        beforeSend: function () {
            editContainer.html('<div class="col-xs-12 col-sm-10"><div class="box-product centerText"><i class="fa fa-refresh fa-spin fa-4x"></i></div></div>');
        },
        success: function (response) {
            template = response;
        },
        complete: function () {
            editContainer.html(template);
            setupDate();
            setUploadFile();
            initEditor();
            enabledBtnTop('3');
        }
    });
}

function removeProduct(id, name) {
    var cfrm = confirm("Remove "+name+" ?");
    if (cfrm) {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/adminPanel/product/',
            data: {'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val(), 'remove':'y', 'id':id},
            success: function (response) {
                if (response == "success") {
                    alert("Product "+name+" has been deleted");
                    rdrListProduct('1');
                }
            }
        });
    } else {
        return;
    }
}

function addCategoryChoices() {
    var moreCatCont = $('#row-category'); // combo box category
    var btn = $('.btn-more-category');
    if (moreCatCont.hasClass('hidden')) {
        btn.text('Hide More Category');
        moreCatCont.removeClass('hidden');
    } else {
        btn.text('More Category');
        moreCatCont.addClass('hidden');
    }
}

function showMoreCat () {
    var container = $('#more-category-box');
    var cbCatOption = $('#category').html(); // combo box option
    container.append('<select class="form-control slctMoreCategory mar-btm10">'+cbCatOption+'</select>');
}

function removeMoreCat () {
    var cbMoreCat = $('.slctMoreCategory');
    if (cbMoreCat.length > 1) {
        cbMoreCat.last().remove();
    }
}

function saveMoreCat (idProduct) {
    var listMoreCat = [];
    $('.slctMoreCategory').each(function () {
        if ($(this).val() != '0') {
            listMoreCat.push($(this).val());
        }
    });
    listMoreCat = listMoreCat.join('|');
    $.ajax({
        type:'POST',
        cache: false,
        url: '/adminPanel/product/',
        data: {csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(), addCategory:'y', idProduct:idProduct, listMoreCat:listMoreCat},
        success: function (response) {
            if (response == "success") {
                rdrEditProduct(idProduct);
            }
        }
    });
}

function removeCategoryProduct(event, idCategory, idProduct) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/product/',
        data: {csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(), removeCategory:'y', idCategory:idCategory, idProduct:idProduct},
        success: function (response) {
            if (response == "success") {
                rdrEditProduct(idProduct);
            }
        }
    });
}

function addStock() {
    var tbody = $('#tableStock tbody');
    var template = [
        '<tr>',
            '<td><div class="form-group"><input type="text" class="form-control stock" /></div></td>',
            '<td><div class="form-group"><input type="text" class="form-control size" /></div></td>',
            '<td><button type="button" class="btn btn-danger center-block" onclick="removeStock(this)"><i class="fa fa-trash"><i/></button></td>',
        '</tr>'
    ].join('');
    tbody.append(template);
}

function removeStock(obj) {
    var rowStock = obj.parentElement.parentElement.remove();
}

function removeImage(id, imgName) {
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/product/',
        data: {csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(), removeImage:'y', id:id, name:imgName},
        success: function (response) {
            if (response == "success"){
                rdrEditProduct(id);
            }
        }
    });
}

function updateProduct (ev, id) {
    ev.preventDefault();
    var data = prepareDataProduct();
    data.id = id;
    data.csrfmiddlewaretoken = $('input[name=csrfmiddlewaretoken]').val();
    data.update = 'y';
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/adminPanel/product/',
        data: data,
        success: function (response) {
            if (response == "success") {
                rdrListProduct(getCookie("page"));
            }
        }
    });
}

function setupDate() {
    var t = new Date();
    var timeDisplay = ['',t.getMonth()+1,'', '/', '',t.getDate(),'', '/', '',t.getFullYear(),''].join('');
    if (document.getElementById('dateStock')) {
        $('#dateStock').val(timeDisplay);
    }
    if (document.getElementById('dateDiscount')) {
        $('#dateDiscount').val(timeDisplay);
        $('#date-discount').datetimepicker({
            format: 'mm/dd/yyyy',
            startDate: new Date(),
            fontAwesome: true,
            pickerPosition: 'bottom-left',
            todayBtn: true,
            minView: 2,
            autoclose: true
        });
    }
}

function setUploadFile() {
    var url = '/adminPanel/product/';
    var counter = 1, images = '';
    var uploadButton = $('<button/>');
    var listID = $('input[name=listID]');
    var ID = [];
    if (listID.length > 1) {
        var i;
        for (i=0; i<listID.length; i++) {
            ID.push(listID[i].value);
        }
        ID = ID.join('|');
    } else {
        ID = listID[0].value;
    }
    uploadButton.addClass('btn btn-success');
    uploadButton.html('<i class="fa fa-upload fa-lg"></i>');
    uploadButton.on('click', function () {
        var _this = $(this),
                data = _this.data();
        _this.prop('disabled', true);
        _this.text('Uploading..');
        data.submit().always(function () {
            _this.remove();
        });
    });
    $('#fileUpload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        previewMaxWidth: 100,
        previewMinWidth: 100,
        add: function (e, data) {
            var acceptFileTypes = /^image\/(gif|jpe?g|png)$/i;
            var maxSizeFile = 2000000;
            if (!acceptFileTypes.test(data.originalFiles[0]['type'])) {
                alert("FILE TYPE NOT ACCEPTED");
                return;
            }
            if (data.originalFiles[0]['size'] > maxSizeFile) {
                alert("FILE SIZE CANNOT MORE THAN 1MB");
                return;
            }
            data.imgID = ['img', counter.toString()].join('');
            data.formData = {listID:ID, csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()};
            counter += 1;
            $.each(data.files, function (index, file) {
                setThumbnail(uploadButton, data);
            });
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#' + data.imgID + '').attr('src', e.target.result);
            };
            reader.readAsDataURL(data.files[0]);
        },

        always: function (e, data) {
            $('#' + data.imgID + '').next().prepend('<p>' + data.files[0].name + '</p>');
            $('#' + data.imgID + '').next().children(':last-child').addClass('hidden');
            alert("Image " + data.files[0].name + " Uploaded");
        }
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
}

function setThumbnail(button, data) {
    var container = $('#listImage'),
    thumbnail = $('<div/>').addClass('thumbnailReview'),
    img = $('<img />').attr('src', '/Static/images/loading.gif').attr('id', data.imgID),
    caption = $('<div/>').addClass('thumbnailCaption'),
    captionBtn = $('</p>'),
    btnUpload = button.clone(true).data(data),
    btnDelete = $('<button/>');
    btnDelete.addClass('btn btn-danger');
    btnDelete.html('<i class="fa fa-trash"></i>');
    btnDelete.attr('type', 'button');
    btnDelete.css('margin-left', '10px');
    btnDelete.on('click', function () {
        deleteImage(data.imgID);
    });

    btnUpload.appendTo(captionBtn);
    btnDelete.appendTo(captionBtn);
    captionBtn.appendTo(caption);
    img.appendTo(thumbnail);
    caption.appendTo(thumbnail);
    thumbnail.appendTo(container);
}

function deleteImage(id) {
    $('#'+id+'').parent().remove();
}
// end page product