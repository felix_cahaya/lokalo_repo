$(document).foundation();

  /* STICKY HEADER*/
  $('.title-bar').on('sticky.zf.stuckto:top', function() {
      $(this).addClass('shrink');

      $('.menu-fixed').css('display','block'); //show scrool down


      $('.menu-normal').css('display','none');

      $('.search-fixed').css('display','block');
      $('.search-normal').css('display','none');

      $('.hide-text').css('display','none');

  }).on('sticky.zf.unstuckfrom:top', function() {
      $(this).removeClass('shrink');
      $('.menu-fixed').css('display','none'); // hide scroll up
      $('.menu-normal').css('display','block');

      $('.search-fixed').css('display','none');
      $('.search-normal').css('display','block');

      $('.hide-text').css('display','initial');

  });

  /* QTY-REVIEW  PRODUK*/
  $(".quantity").on("click", function() {

  	var $button = $(this);

  	if ($button.text() == "+") {
  		var op = $(this).attr('plus');

  		var oldValue = $("#qty_"+op).val();
  		var oldTotalQty = $("#in_"+op).val();

  		var newVal = parseFloat(oldValue) + 1;
  		var newTotal = (oldTotalQty / oldValue) * newVal;
  	} else {
  		var op = $(this).attr('minus');
  		var oldValue = $("#qty_"+op).val();
  		var oldTotalQty = $("#in_"+op).val();

  		if (oldValue > 1) {
  			var newVal = parseFloat(oldValue) - 1;
  			var newTotal = (oldTotalQty / oldValue) * newVal;
  		} else {
  			newVal = 1;
  			newTotal = oldTotalQty;
  		}
  	}

  	$button.closest('.sp-quantity').find("input.quntity-input").val(newVal);

  	$("#in_"+op).val(newTotal);
  	if(op == 1){
  		$('.qty-total-1').text(formatQty(newTotal));
  	} else if(op == 2){
  		$('.qty-total-2').text(formatQty(newTotal));
  	}

  	//ambil
  	var tot_1 = $('#in_1').val();
  	var tot_2 = $('#in_2').val();
  	// hitung
  	var subtotal = parseFloat(tot_1)+parseFloat(tot_2);
  	//tampil
  	$("#sub_tot").val(subtotal); //tampil input
  	$('.subtotal').text(formatQty(subtotal)); // tampil td

  	//ambil id
  	var subtot = $("#sub_tot").val();
  	var kirim = $("#biaya_kirim").val();
  	//hitung
  	var total = parseFloat(subtot)+parseFloat(kirim);
  	//tampil
  	$('#bayar_tot').val(formatQty(total));
  	$('.total_bayar').text(formatQty(total));
  });

  function formatQty(val) {
  	return 'Rp. ' + val.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") + ',-';
  }

  /* JS PROFIL & KERANJANG */
  function profil(id) {
  	 var e = document.getElementById(id);
  	if(e.style.visibility == 'visible')
  		e.style.visibility = 'hidden';
  	else
  	  e.style.visibility = 'visible';
  	  $('#keranjang').css('visibility','hidden');
  }

  function keranjang(id) {
  	 var e = document.getElementById(id);
  	if(e.style.visibility == 'visible')
  		e.style.visibility = 'hidden';
  	else
  	  e.style.visibility = 'visible';
  	  $('#profil').css('visibility','hidden');
  }

  /* DETAIL PRODUK SLIDER IMG*/
  $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
          animation: "fade", // ini
          controlNav: false,
          directionNav: true,
          slideshow: false,
          itemWidth: 180, // ini
          asNavFor: '#slider',
          maxItems: 3,
          prevText: "",
          nextText: ""
      });

      $('#slider').flexslider({
          animation: "slide",
          controlNav: false,
          directionNav: true,
          slideshow: false,
          sync: "#carousel",
          prevText: "",
          nextText: ""
      });
  });

  /* DETAIL PRODUK ZOOM*/
$(window).load(function(){
	$(".zoom").elevateZoom({
		easing: true,
		containLensZoom: true,
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 500,
		lensFadeIn: 500,
		lensFadeOut: 500,
		zoomWindowWidth: 469,
		zoomWindowHeight: 400
	});
}); 