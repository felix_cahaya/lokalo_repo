/**
 * Created by Felix on 2/28/2016.
 */

$(document).ready(function () {
    var objCat, objSubCat, objChildCat;
    $.ajax({
        type: 'GET',
        cache: false,
        url: '/',
        success: function (data) {
            console.log(data);
        }
    });
});

function login () {
    var email = $('input[name=email]').val();
    var password = $('#password').val();
    $.ajax({
        type: 'POST',
        cache: false,
        url: '/login-suggestion/',
        data: {csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(), email:email, password:password},
        success: function (response) {
            if (response == "success") {
                window.location.href = '/adminPanel/';
            }
        }
    });
}

/*
$(document).ready(function () {
    $('.product-card').each(function () {
        $(this).mouseenter(function () {
            $(this).find('.card-option').css({
                "bottom":"0",
                "opacity":"1"
            });
        });
    });

    $('.product-card').each(function () {
        $(this).mouseleave(function () {
            $(this).find('.card-option').css({
                "bottom":"-20px",
                "opacity":"0",
                "-webkit-transition": "all 0.4s",
                "moz-transition": "all 0.4s",
                "transition": "all 0.4s"
            });
        });
    });

    $('.add-cart-option').each(function () {
        $(this).mouseenter(function () {
            $(this).css("background-color","#42a5f5");
            $(this).find('a').css("color","white");
        });
    });

    $('.add-cart-option').each(function () {
        $(this).mouseleave(function () {
            $(this).css("background-color","white");
            $(this).find('a').css("color","black");
        });
    });

    $('.detail-option').each(function () {
        $(this).mouseenter(function () {
            $(this).css("background-color","#42a5f5");
            $(this).find('a').css("color","white");
        });
    });

    $('.detail-option').each(function () {
        $(this).mouseleave(function () {
            $(this).css("background-color","white");
            $(this).find('a').css("color","black");
        });
    });
});
*/