from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'lokalo.views.home', name='home'),
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.urls')),
    url(r'^adminPanel/', include('adminApps.urls')),
    url(r'^supplier/', include('supplierApps.urls')),
]
