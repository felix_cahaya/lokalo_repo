__author__ = 'Felix'

from django.conf.urls import url
from adminApps import views

urlpatterns = [
    url(r'^$', views.signin, name='adminSignin'),
    url(r'^signout/$', views.signout, name='adminSignout'),
    url(r'^dashboard/$', views.admin, name='admin'),
    url(r'^supplier/$', views.adminSupplier, name='adminSupplier'),
    url(r'^category/$', views.adminCategory, name='adminCategory'),
    url(r'^product/$', views.adminProduct, name='adminProduct'),
    url(r'^shipping/$', views.adminShipping, name='adminShipping'),
    url(r'^email/$', views.adminEmail, name='adminEmail'),
]