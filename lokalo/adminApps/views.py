__author__ = 'Felix'

# django core
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import get_template
from django.contrib import auth
from django.contrib.auth import logout
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.dispatch import receiver

# mongoengine user
from mongoengine.django.auth import User

# django models
from adminApps.models import *
from supplierApps.models import SupplierInfo

# django form
from lokaloForm.adminForm import *

# python package
import os
import datetime
import requests

loginUrl = '/adminPanel/'

def signin(request):
    form = SigninForm()
    if request.POST:
        form = SigninForm(request.POST)
        if form.is_valid():
            dataForm = form.cleaned_data
            try:
                user = User.objects.get(username=dataForm['username'])
                if user.check_password(dataForm['password']):
                    validUser = auth.authenticate(username=dataForm['username'], password=dataForm['password'])
                    auth.login(request, validUser)
                    d = {'status':'success'}
                    return HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signin(data);</script>")
                else:
                    d = {'status':'failed', 'info':'401'}
                    response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signin(data);</script>")
                    response["Error"] = "Invalid Password"
                    return response
            except Exception, err:
                d = {'status':'failed', 'info':'402'}
                response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signin(data);</script>")
                response["Error"] = "Username Doesn't Exist"
                return response
    return render(request, 'admin/signin.html', {'form':form})

def signout(request):
    logout(request)
    return HttpResponseRedirect('/adminPanel/')

@login_required(login_url=loginUrl)
def admin(request):
    if request.user.is_superuser:
        return render(request, 'admin/admin.html', )

@login_required(login_url=loginUrl)
def adminSupplier(request):
    # status error
    # 401 = username already exist
    if request.user.is_superuser:
        listSupplier = SupplierInfo.objects.all()
        form = AddSupplier()
        objTemp = {
            'form':form,
            'listSup':listSupplier
        }
        d = {}
        if request.POST:
            if request.POST.has_key('add-supplier'):
                form = AddSupplier(request.POST)
                if form.is_valid():
                    dataForm = form.cleaned_data
                    try:
                        SupplierInfo.objects.get(sup_email=dataForm['email'])
                        d['status'] = 'failed'
                        d['info'] = '401'
                        response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.addSupResponse(data);</script>")
                        response['Error'] = 'Account Already Exist'
                        return response
                    except Exception, err:
                        new_user = User.create_user(username=dataForm['email'], password=dataForm['password'], email=dataForm['email'])
                        new_user.first_name = dataForm['first_name']
                        new_user.last_name = dataForm['last_name']
                        new_sup = SupplierInfo(account=new_user, sup_email=dataForm['email'])
                        new_sup.name = new_user.get_full_name()
                        new_user.save()
                        new_sup.save()
                        d['status'] = 'success'
                        response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.addSupResponse(data);</script>")
                        return response
                    return HttpResponse("success")

            if request.POST.has_key('del-supplier'):
                print request.POST
                email = request.POST['sup_email']
                try:
                    user = User.objects.get(email=email)
                    supplier = SupplierInfo.objects.get(sup_email=email)
                    user.delete()
                    supplier.delete()
                    d['status'] = 'success'
                    response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.deleteSupResponse(data);</script>")
                    return response
                except Exception, err:
                    d['status'] = 'failed'
                    response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.deleteSupResponse(data);</script>")
                    response['Error'] = err
                    return response
        return render(request, 'admin/supplier.html', objTemp)

@login_required(login_url=loginUrl)
def adminCategory(request):
    if request.user.is_superuser:
        if request.GET:
            if request.GET.has_key("chgContent"):
                return render(request, 'admin/category.html')

            if request.GET.has_key("add"):
                return render(request, 'admin/addCategory.html')

            if request.GET.has_key("edit"):
                ID = request.GET["id"]
                levelCategory = int(request.GET["level"])
                objCategory = getLvlCategory(levelCategory)
                object = objCategory.objects.get(id=ID)
                categoryName = object.name
                description = object.description
                publish = object.published
                published = False
                if publish == 'true':
                    published = True
                objTemp = {
                    'ID':ID,
                    'levelCategory':levelCategory,
                    'categoryName':categoryName,
                    'description':description,
                    'published':published
                }
                return render(request, 'admin/editCategory.html', objTemp)

            if request.GET.has_key("listCategory"):
                listCategory = Category1.objects.all()
                objTemp = {
                    "listCategory" : listCategory
                }
                return render(request, 'admin/listCategory.html', objTemp)

            if request.GET.has_key("listSubCategory"):
                ID = request.GET["id"]
                levelCategory = int(request.GET["level"])
                objCategory = getLvlCategory(levelCategory)
                listSubCategory = objCategory.objects.get(id=ID).subcategory
                objTemp = {
                    "listCategory" : listSubCategory
                }
                return render(request, 'admin/listCategory.html', objTemp)

        if request.POST:
            print request.POST
            if request.POST.has_key("add"):
                categoryName = request.POST["categoryName"]
                description = request.POST["description"]
                publish = request.POST["publish"]
                levelCategory = int(request.POST["level"])
                if request.POST["addSubCategory"] == '1':
                    parentId = request.POST["parentId"]
                    newSubCatLevel = levelCategory + 1
                    objCategory = getLvlCategory(newSubCatLevel)
                    objParentCategory = getLvlCategory(levelCategory)
                    objParentCategory = objParentCategory.objects(id=parentId)
                    try:
                        newSubCatObj = objCategory(name=categoryName, description=description, published=publish, level_category=newSubCatLevel, parent=objParentCategory[0])
                        newSubCatObj.save()
                        if newSubCatLevel == 3:
                            newSubCatObj.grand_parent = objParentCategory[0].parent
                            newSubCatObj.save()
                        objParentCategory.update_one(push__subcategory=newSubCatObj)
                        return HttpResponse("success")
                    except Exception, err:
                        response = HttpResponse("failed")
                        response["Error"] = err
                        return response
                else:
                    try:
                        newCatObj = Category1(name=categoryName, description=description, published=publish, subcategory=[], level_category=levelCategory)
                        newCatObj.save()
                        return HttpResponse('success')
                    except Exception, err:
                        response = HttpResponse("failed")
                        response["Error"] = err
                        return response

            if request.POST.has_key("update"):
                try:
                    ID = request.POST["id"]
                    name = request.POST["name"]
                    description = request.POST["description"]
                    publish = request.POST["publish"]
                    levelCategory = int(request.POST['level'])
                    objCategory = getLvlCategory(levelCategory)
                    objCategory = objCategory.objects.get(id=ID)
                    objCategory.name = name
                    objCategory.description = description
                    objCategory.published = publish
                    objCategory.save()
                    return HttpResponse("success")
                except Exception, err:
                    response = HttpResponse("failed")
                    response["Error"] = err
                    return response

            if request.POST.has_key("remove"):
                ID = request.POST["id"]
                levelCategory = int(request.POST["level"])
                objCategory = getLvlCategory(levelCategory)
                objCategory = objCategory.objects.get(id=ID)
                print objCategory
                if levelCategory != 3:
                    if len(objCategory.subcategory) != 0:
                        for subCat in objCategory.subcategory:
                            try:
                                if len(subCat.subcategory) != 0:
                                    for sub in subCat.subcategory:
                                        sub.delete()
                            except:
                                pass
                            subCat.delete()
                        objCategory.delete()
                    else:
                        objCategory.delete()
                else:
                    objCategory.delete()
                return HttpResponse("success")
    return render(request, 'admin/category.html')

@login_required(login_url=loginUrl)
def adminProduct(request):
    if request.user.is_superuser:
        if request.FILES:
            print request.FILES
            if request.FILES.has_key('myFiles[]'):
                img = request.FILES["myFiles[]"]
                imgBinary = img.read()
                id = request.COOKIES['rdrEditProduct']
                listIDCat = request.POST['listID'].split('|')
                imgDir = os.path.join(os.path.abspath(os.curdir), "Static", "images")
                if not os.path.exists(os.path.join(imgDir, "Product")):
                    os.mkdir(os.path.join(imgDir, "Product"))
                curDir = os.path.join(imgDir, "Product")
                for idCat in listIDCat:
                    objProduct = Product.objects.get(id=id)
                    objCategory = Category3.objects.get(id=idCat)
                    if objCategory.name not in os.listdir(curDir):
                        os.mkdir(os.path.join(curDir, objCategory.name))
                    catDir = os.path.join(curDir, objCategory.name)
                    if objProduct.product_name not in os.listdir(catDir):
                        os.mkdir(os.path.join(catDir, objProduct.product_name))
                    productDir = os.path.join(catDir, objProduct.product_name)
                    open (productDir+'/'+img.name, 'wb').write(imgBinary)
                    if img.name not in objProduct.images:
                        objProduct.images.append(img.name)
                    objProduct.save()
                return HttpResponse("success")

        if request.GET:
            listCategory = Category3.objects(level_category=3)
            objTemp = {
                'listCategory' : listCategory
            }
            if request.GET.has_key('chgContent'):
                return render(request, 'admin/product.html', objTemp)

            if request.GET.has_key('add-product'):
                return  render(request, 'admin/addProduct.html', objTemp)

            if request.GET.has_key('list-product'):
                page = int(request.GET['page'])
                listProduct = Product.objects.all()
                paginatorObj = Paginator(listProduct, 2)
                lastPage = paginatorObj.num_pages
                isLastPage = False
                isFirstPage = False
                pageObj = paginatorObj.page(page)
                listObj = pageObj.object_list
                currentPage = pageObj.number
                hasPrev = pageObj.has_previous()
                hasNext = pageObj.has_next()
                prevPage = int
                nextPage = int
                if hasPrev == True:
                    prevPage = pageObj.previous_page_number()
                if hasNext == True:
                    nextPage = pageObj.next_page_number()
                if currentPage == lastPage:
                    isLastPage = True
                if currentPage == 1:
                    isFirstPage = True
                objTemp = {
                    'listProduct' : listObj,
                    'currentPage' : currentPage,
                    'hasPrev' : hasPrev,
                    'hasNext' : hasNext,
                    'prevPage' : prevPage,
                    'nextPage' : nextPage,
                    'lastPage' : lastPage,
                    'isFirst' : isFirstPage,
                    'isLast' : isLastPage
                }
                return render(request, 'admin/listProduct.html', objTemp)

            if request.GET.has_key('edit-page'):
                id = request.GET['id']
                productObj = Product.objects.get(id=id)
                listCategory = Category3.objects.all()
                objTemp = {
                    'product' : productObj,
                    'listCategory' : listCategory
                }
                return render(request, 'admin/editProduct.html', objTemp)

        if request.POST:
            print request.POST
            if request.POST.has_key('save'):
                productCode = request.POST['productCode']
                productName = request.POST['productName']
                try:
                    objProduct = Product.objects.get(product_code=productCode)
                except Exception:
                    try:
                        objProductName = Product.objects.get(product_name=productName)
                    except Exception:
                        pass
                    else:
                        return HttpResponse("Duplicate Product Name")
                    category = request.POST['category']
                    listCat = category.split('|')
                    brand = request.POST['brand']
                    color = request.POST['color']
                    price = float(request.POST['price'])
                    stock = request.POST['ss'].split('|')
                    arrDictStock = []
                    dictStock = {}
                    if len(stock) > 1: #Multi Stock
                        c = 0
                        for i in stock:
                            dictStock = {}
                            dataStock = stock[c].split('~')
                            dictStock = {'stock':dataStock[0], 'size':dataStock[1]}
                            arrDictStock.append(dictStock)
                            c += 1
                    else:
                        dataStock = stock[0].split('~')
                        dictStock = {'stock':dataStock[0], 'size':dataStock[1]}
                        arrDictStock.append(dictStock)
                    lastUpdate = request.POST['lastUpdate'].split('/')
                    dateUpdate = datetime.datetime(int(lastUpdate[2]), int(lastUpdate[0]), int(lastUpdate[1]))
                    discount = 0
                    weight = request.POST['weight']
                    weightName = request.POST['weightName']
                    description = request.POST['description']
                    if request.POST.has_key('discount'):
                        discount = float(request.POST['discount'])
                        newPrice = float(request.POST['newPrice'])
                        discExp = request.POST['discExp'].split('/')
                        expired = datetime.datetime(int(discExp[2]), int(discExp[0]), int(discExp[1]))
                    newProduct = Product(product_code=productCode, product_name=productName, brand=brand, color=color, stock=arrDictStock, last_update=dateUpdate, price=price, weight=weight, weight_unit=weightName, description=description)
                    newProduct.save()
                    if discount != 0:
                        newProduct.discount = discount
                        newProduct.new_price = newPrice
                        newProduct.expired_discount = expired
                        newProduct.save()
                    listIDCat = []
                    dictCategory = {'listCategory':[], 'listID':[]}
                    for cat in listCat:
                        objCategory = Category3.objects(id=cat)
                        objParentCat = Category2.objects(id=objCategory[0].parent.id)
                        objGrandParentCat = Category1.objects(id=objParentCat[0].parent.id)
                        objNewProduct = Product.objects.get(id=newProduct.id)
                        dataCategory = {'id':cat, 'grandParent':objGrandParentCat[0].name, 'parent':objParentCat[0].name, 'categoryName':objCategory[0].name }
                        listIDCat.append(cat)
                        dictCategory['listCategory'].append(dataCategory)
                        dictCategory['listID'].append(cat)
                        objNewProduct.category = dictCategory
                        objNewProduct.save()
                        objCategory.update_one(push__list_product=newProduct)
                        objParentCat.update_one(push__list_product=newProduct)
                        objGrandParentCat.update_one(push__list_product=newProduct)
                    return HttpResponse("success")
                else:
                    return HttpResponse("Duplicate Product Code")

            if request.POST.has_key('update'):
                print request.POST
                idProduct = request.POST['id']
                category = request.POST['category']
                productCode = request.POST['productCode']
                productName = request.POST['productName']
                brand = request.POST['brand']
                color = request.POST['color']
                price = float(request.POST['price'])
                listCat = category.split('|')
                stock = request.POST['ss'].split('|')
                arrDictStock = []
                dictStock = {}
                if len(stock) > 1: #Multi Stock
                    c = 0
                    for i in stock:
                        dictStock = {}
                        dataStock = stock[c].split('~')
                        dictStock = {'stock':dataStock[0], 'size':dataStock[1]}
                        arrDictStock.append(dictStock)
                        c += 1
                else:
                    dataStock = stock[0].split('~')
                    dictStock = {'stock':dataStock[0], 'size':dataStock[1]}
                    arrDictStock.append(dictStock)
                lastUpdate = request.POST['lastUpdate'].split('/')
                dateUpdate = datetime.datetime(int(lastUpdate[2]), int(lastUpdate[0]), int(lastUpdate[1]))
                discount = 0
                weight = request.POST['weight']
                weightName = request.POST['weightName']
                description = request.POST['description']

                objProduct = Product.objects.get(id=idProduct)
                objProduct.product_code = productCode
                objProduct.product_name = productName
                objProduct.brand = brand
                objProduct.color = color
                objProduct.stock = arrDictStock
                objProduct.last_update = dateUpdate
                objProduct.price = price
                objProduct.weight = weight
                objProduct.weight_unit = weightName
                objProduct.description = description

                if request.POST.has_key('discount'):
                    discount = float(request.POST['discount'])
                    newPrice = float(request.POST['newPrice'])
                    discExp = request.POST['discExp'].split('/')
                    expired = datetime.datetime(int(discExp[2]), int(discExp[0]), int(discExp[1]))
                    objProduct.discount = discount
                    objProduct.new_price = newPrice
                    objProduct.expired_discount = expired

                objProduct.save()
                for cat in listCat:
                    objCategory = Category3.objects(id=cat)
                    objParentCat = Category2.objects(id=objCategory[0].parent.id)
                    objGrandParentCat = Category1.objects(id=objParentCat[0].parent.id)
                    objNewProduct = Product.objects.get(id=objProduct.id)
                    listCatID = objNewProduct.category['listID']
                    listCategory = objNewProduct.category['listCategory']
                    if cat not in listCatID:
                        listCatID.append(cat)
                        listCategory.append({'id':cat,'grandParent':objGrandParentCat[0].name, 'parent':objParentCat[0].name, 'categoryName':objCategory[0].name})
                        objNewProduct.category['listID'] = listCatID
                        objNewProduct.category['listCategory'] = listCategory
                        objNewProduct.save()
                        objCategory.update_one(push__list_product=objNewProduct)
                        objParentCat.update_one(push__list_product=objNewProduct)
                        objGrandParentCat.update_one(push__list_product=objNewProduct)
                return HttpResponse("success")

            if request.POST.has_key('remove'):
                id = request.POST['id']
                objProduct = Product.objects.get(id=id)
                removeFileAndFolder(objProduct.category['listID'], objProduct.product_name)
                objProduct.delete()
                return HttpResponse("success")

            if request.POST.has_key('updateImg'):
                id = request.POST['id']
                imageName = request.POST['name']
                productObj = Product.objects.get(id=id)
                productObj.main_image = imageName
                productObj.save()
                return HttpResponse("success")

            if request.POST.has_key('removeImage'):
                print request.POST
                id = request.POST['id']
                name = request.POST['name']
                objProduct = Product.objects.get(id=id)
                objProduct.images.remove(name)
                if name == objProduct.main_image:
                    if len(objProduct.images) > 0:
                        objProduct.main_image = objProduct.images[0]
                    else:
                        objProduct.main_image = ''
                objProduct.save()
                removeFile(objProduct.category['listID'], objProduct.product_name, name)
                return HttpResponse("success")

            if request.POST.has_key('addCategory'):
                idProduct = request.POST['idProduct']
                listMoreCat = request.POST['listMoreCat'].split('|')
                objProduct = Product.objects.get(id=idProduct)
                for cat in listMoreCat:
                    if cat not in objProduct.category['listID']:
                        objCategory = Category3.objects(id=cat)
                        objParentCat = Category2.objects(id=objCategory[0].parent.id)
                        objGrandParentCat = Category1.objects(id=objParentCat[0].parent.id)
                        catInfo = {'id':cat, 'grandParent':objGrandParentCat[0].name, 'parent':objParentCat[0].name, 'categoryName':objCategory[0].name}
                        objProduct.category['listID'].append(cat)
                        objProduct.category['listCategory'].append(catInfo)
                        objCategory.update_one(push__list_product=objProduct)
                        objParentCat.update_one(push__list_product=objProduct)
                        objGrandParentCat.update_one(push__list_product=objProduct)
                        objProduct.save()
                        categoryName = objCategory[0].name
                        for name in objProduct.images:
                            prodDir = os.path.join(os.path.abspath(os.curdir), 'Static', 'images', 'Product')
                            idProdCat = objProduct.category['listID'][0] #pick 1 category from product
                            objProdCat = Category3.objects.get(id=idProdCat)
                            img = open(os.path.join(prodDir, objProdCat.name, objProduct.product_name, name), 'rb').read() #open existing image file
                            if categoryName not in os.listdir(prodDir):
                                os.mkdir(os.path.join(prodDir, categoryName))
                            catDir = os.path.join(prodDir, categoryName)
                            if objProduct.product_name not in os.listdir(catDir):
                                os.mkdir(os.path.join(catDir, objProduct.product_name))
                            itemDir = os.path.join(catDir, objProduct.product_name)
                            open(os.path.join(itemDir, name), 'wb').write(img)
                return HttpResponse("success")

            if request.POST.has_key('removeCategory'):
                idCategory = request.POST['idCategory']
                idProduct = request.POST['idProduct']
                objProduct = Product.objects.get(id=idProduct)
                c = 0
                for cat in objProduct.category['listID']:
                    if cat == idCategory:
                        objProduct.category['listID'].remove(cat)
                        objProduct.category['listCategory'].pop(c)
                        objProduct.save()
                    c += 1
                objCategory = Category3.objects(id=idCategory)
                parentCategory = Category2.objects(id=objCategory[0].parent.id)
                grandParentCategory = Category1.objects(id=objCategory[0].grand_parent.id)
                objCategory.update_one(pull__list_product=objProduct)
                parentCategory.update_one(pull__list_product=objProduct)
                grandParentCategory.update_one(pull__list_product=objProduct)
                removeFileAndFolder([idCategory], objProduct.product_name)
                return HttpResponse("success")

        return render(request, 'admin/product.html')

@login_required(login_url=loginUrl)
def adminShipping(request):
    if request.user.is_superuser:
        dataTemp = {}
        dataProvinceFromRO = requests.get('http://api.rajaongkir.com/starter/province?key=82a1a2d3b3314b8bf7e95ddb713018a6')
        respProvinceRO = dataProvinceFromRO.json()
        dataRespProvince = respProvinceRO['rajaongkir']
        if dataRespProvince['status']['code'] == 200:
            dataProvince = dataRespProvince['results']
            dataTemp['listProvince'] = dataProvince

        dataCityFromRO = requests.get('http://api.rajaongkir.com/starter/city?key=82a1a2d3b3314b8bf7e95ddb713018a6')
        respCityRO = dataCityFromRO.json()
        dataRespCity = respCityRO['rajaongkir']
        if dataRespCity['status']['code'] == 200:
            dataCity = dataRespCity['results']
            dataTemp['listCity'] = dataCity
        return render(request, 'admin/shipping.html', dataTemp)

@login_required(login_url=loginUrl)
def adminEmail(request):
    if request.user.is_superuser:
        data = {'site':'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'}
        if request.POST:
            print request.POST
            to = request.POST['toEmail']
            subject = request.POST['subjectEmail']
            message = request.POST['messasgeEmail']
            data = {'site':'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'}
            context = RequestContext(request, data)
            emailContent = get_template('admin/Email.html').render(context)
            mail = EmailMessage(subject, emailContent, 'felixcahaya18@gmail.com', [to])
            mail.content_subtype = "html"
            mail.send(fail_silently=False)
    return render(request, 'admin/shipping.html', data)

# helper function
def getLvlCategory(index):
    if index == 1:
        return Category1
    elif index == 2:
        return Category2
    else:
        return Category3

def removeFileAndFolder(listCat, prodName):
    prodName = str(prodName)
    imgDir = os.path.join(os.path.abspath(os.curdir), 'Static', 'images', 'Product')
    for cat in listCat:
        objCategory = Category3.objects.get(id=cat)
        prodDir = os.path.join(imgDir, objCategory.name, prodName)
        for path,dir,files in os.walk(prodDir):
            if len(files) != 0:
                for name in files:
                    os.remove(os.path.join(path, name))
            os.rmdir(path)

def removeFile(listCat, prodName, imgName):
    prodName = str(prodName)
    name = str(imgName)
    imgDir = os.path.join(os.path.abspath(os.curdir), 'Static', 'images', 'Product')
    for cat in listCat:
        objCategory = Category3.objects.get(id=cat)
        prodDir = os.path.join(imgDir, objCategory.name, prodName)
        for path,dir,files in os.walk(prodDir):
            if imgName in files:
                os.remove(os.path.join(path, imgName))