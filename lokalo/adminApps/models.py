import mongoengine
from mongoengine.django.auth import User
from mongoengine import *

class Product(Document):
    product_code = StringField(max_length=50, required=True, unique=True)
    product_name = StringField(max_length=100, required=True)
    brand = StringField(max_length=50, required=True)
    color = StringField(max_length=20, required=False)
    stock = ListField(DictField(required=True))
    last_update = DateTimeField(required=True)
    price = IntField(min_value=0, required=True)
    discount = IntField(min_value=0, default=0, required=False)
    new_price = IntField(min_value=0, default=0, required=False)
    expired_discount = DateTimeField(required=False)
    weight = StringField(max_length=10, required=True)
    weight_unit = StringField(max_length=10, required=True)
    images = ListField(StringField(max_length=100, required=False))
    main_image = StringField(max_length=50, required=False)
    category = DictField(required=False)
    description = StringField(max_length=5000, required=False)

class Category3(Document):
    name = StringField(unique=True, max_length=50, required=True)
    description = StringField(max_length=200, required=False)
    published = StringField(required=True, default='false')
    level_category = IntField(required=True)
    parent = ReferenceField('Category2', required=True)
    grand_parent = ReferenceField('Category1', required=False)
    list_product = ListField(ReferenceField('Product', reverse_delete_rule=mongoengine.PULL), required=False)

class Category2(Document):
    name = StringField(unique=True, max_length=50, required=True)
    description = StringField(max_length=200, required=False)
    published = StringField(required=True, default='false')
    subcategory = ListField(ReferenceField('Category3', reverse_delete_rule=mongoengine.PULL), required=False, default=[])
    level_category = IntField(required=True)
    parent = ReferenceField('Category1', required=True)
    list_product = ListField(ReferenceField('Product', reverse_delete_rule=mongoengine.PULL), required=False)

class Category1(Document):
    name = StringField(unique=True, max_length=50, required=True)
    description = StringField(max_length=200, required=False)
    published = StringField(required=True, default='false')
    subcategory = ListField(ReferenceField('Category2', reverse_delete_rule=mongoengine.PULL), required=False, default=[])
    level_category = IntField(required=True)
    list_product = ListField(ReferenceField('Product', reverse_delete_rule=mongoengine.PULL), required=False)