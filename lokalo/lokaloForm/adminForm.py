__author__ = 'Felix'

from django import forms

class SigninForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'id':'username', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Admin Username', 'required':True
    }), label='Username')
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'id':'password', 'class':'form-control', 'oninvalid':'checkError(this)', 'placeholder':'Password', 'required':True
    }), label='Password')

class AddSupplier(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={
        'id':'first', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'First Name', 'required':True
    }), label='First Name')
    last_name = forms.CharField(widget=forms.TextInput(attrs={
        'id':'last', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Last Name', 'required':True
    }), label='Last Name')
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'id':'email', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'supplier@supplier.com', 'required':True
    }), label='Email Supplier')
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'id':'password', 'class':'form-control', 'oninvalid':'checkError(this)', 'placeholder':'Password', 'required':True
    }), label='Password')
    repass = forms.CharField(widget=forms.PasswordInput(attrs={
        'id':'re-password', 'class':'form-control', 'oninvalid':'checkError(this)', 'placeholder':'Repeat Password', 'required':True
    }), label='Repeat Password')