__author__ = 'Felix'

from django import forms
from django.forms import ModelForm

class SigninForm(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'id':'email', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Email Anda Yang Terdaftar Di Lokalo', 'required':True
    }), label='Email')
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'id':'password', 'class':'form-control', 'oninvalid':'checkError(this)', 'placeholder':'Password', 'required':True
    }), label='Kata Sandi')

class SignupForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'id':'name', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Nama Anda', 'required':True
    }), label='Nama')
    username = forms.CharField(widget=forms.TextInput(attrs={
        'id':'username', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Nama Akun Anda', 'required':True
    }), label='Nama Akun')
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'id':'email', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Email Anda', 'required':True
    }), label='Email')
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'id':'password', 'class':'form-control', 'oninvalid':'checkError(this)', 'placeholder':'Kata Sandi', 'required':True
    }), label='Kata Sandi')
    repassword = forms.CharField(widget=forms.PasswordInput(attrs={
        'id':'repassword', 'class':'form-control', 'oninvalid':'checkError(this)', 'placeholder':'Ulangi Kata Sandi', 'required':True
    }), label='Ulangi Kata Sandi')
    gender = forms.ChoiceField(widget=forms.RadioSelect, choices=(('pria', 'Pria',), ('wanita', 'Wanita',)))
    address = forms.CharField(widget=forms.Textarea(attrs={
        'id':'address', 'class':'form-control', 'rows':'3', 'oninvalid':'checkError(this)', 'placeholder':'Alamat Anda', 'required':True, 'style':'resize:none;'
    }), label='Alamat')
    mobile = forms.CharField(widget=forms.TextInput(attrs={
        'id':'mobile', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'placeholder':'Nomor Telepon Anda', 'required':True
    }), label='No Telepon')

class EditProfileForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={
        'id':'first_name', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'required':True
    }), label='Nama Depan')
    last_name = forms.CharField(widget=forms.TextInput(attrs={
        'id':'last_name', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'required':True
    }), label='Nama Belakang')
    email = forms.CharField(widget=forms.EmailInput(attrs={
        'id':'email', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'required':True
    }), label='Email')
    address = forms.CharField(widget=forms.Textarea(attrs={
        'id':'address', 'class':'form-control', 'rows':'3', 'oninvalid':'checkError(this)', 'required':True, 'style':'resize:none;'
    }), label='Alamat')
    mobile = forms.CharField(widget=forms.TextInput(attrs={
        'id':'mobile', 'class':'form-control', 'oninvalid':'checkError(this)', 'autocomplete':'off', 'required':True
    }), label='No Telepon')

