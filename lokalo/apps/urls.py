__author__ = 'Felix'

from django.conf.urls import url
from apps import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^ProductDetail/$', views.redirect, name='redirect'),
    url(r'^ProductDetail/(?P<id>[0-9]+)/$', views.product_detail, name='productDetail'),
    url(r'^login-suggestion/$', views.loginSuggestion, name='loginSuggestion'),
]