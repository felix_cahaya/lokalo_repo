from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render

from adminApps.models import *


def home(request):
    abc = [1,2,3,4]
    listCategory = Category1.objects.all()
    objTemp = {
        'list' : abc,
        'listCategory' : listCategory
    }
    if request.GET:
        return HttpResponse("GET")
    return render(request, 'index.html', objTemp)

def product_detail(request, id):
    return render(request, 'Product-Detail.html')

def loginSuggestion(request):
    if request.POST:
        print request.POST
        goLogin(request)
        return HttpResponse("success")
    return render(request, 'login-sugestion.html')

def redirect(request):
    return HttpResponseRedirect('/')

# ADMIN PAGE

