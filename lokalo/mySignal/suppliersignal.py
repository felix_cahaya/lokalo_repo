__author__ = 'Felix'
from mongoengine import signals
from supplierApps.models import SupplierInfo

def handler(event):
    """Signal decorator to allow use of callback functions as class decorators."""

    def decorator(fn):
        def apply(cls):
            event.connect(fn, sender=cls)
            return cls

        fn.apply = apply
        return fn

    return decorator

@handler(signals.post_delete)
def delete_sup(sender, document):
    email = document.sup_email
    sup = SupplierInfo.objects.get(sup_email=email)
    sup.delete()