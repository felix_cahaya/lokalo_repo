__author__ = 'Felix'

import mongoengine
from mongoengine import *
from mongoengine.django.auth import User
from mongoengine import signals
import datetime

class SupplierInfo(Document):
    account = ReferenceField('User', required=True)
    sup_email = StringField(max_length=20, required=True, unique=True) # sup_email = account.email, unique field
    name = StringField(max_length=100)
    address = StringField(max_length=200, default='-')
    mobile = StringField(max_length=20, default='-')

    def __unicode__(self):
        return self.sup_email