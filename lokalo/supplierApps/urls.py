__author__ = 'Felix'

from django.conf.urls import url
from supplierApps import views

urlpatterns = [
    url(r'^$', views.signin, name='signin'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^signout/$', views.signout, name='signout'),
    url(r'^main/$', views.main, name='main'),
    url(r'^lokalku/$', views.lokalku, name='lokalku'),
    url(r'^product/$', views.product, name='product'),
]