__author__ = 'Felix'

# Django core
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from supplierApps.models import SupplierInfo

# Django Form
from lokaloForm.supplierForm import *

# mongoengine
from mongoengine.django.auth import User

loginUrl = '/supplier/'

def signup(request):
    # error signup
    # 401 = Username Already Exist
    form = SignupForm()
    if request.POST:
        form = SignupForm(request.POST)
        if form.is_valid():
            dataForm = form.cleaned_data
            try:
                user = User.objects.get(username=dataForm['username'])
                d = {'status':'failed', 'info':'401'}
                response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signup(data);</script>")
                response["Error"] = "Username Already Exist"
                return response
            except Exception, err:
                user = User.create_user(username=dataForm['username'], password=dataForm['password'], email=dataForm['email'])
                user.save()
                newSupplier = SupplierInfo(account=user, name=dataForm['name'], gender=dataForm['gender'], address=dataForm['address'], mobile=dataForm['mobile'])
                newSupplier.save()
                d = {'status':'success'}
                return HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signup(data);</script>")
    return render(request, 'supplier/signup.html', {'form':form})

def signin(request):
    # error signin
    # 401 = Invalid Password
    # 402 = Username Doesn't Exist
    form = SigninForm()
    if request.POST:
        form = SigninForm(request.POST)
        if form.is_valid():
            dataForm = form.cleaned_data
            try:
                user = User.objects.get(email=dataForm['email'])
                supplier = SupplierInfo.objects.get(sup_email=dataForm['email'])
                if user.check_password(dataForm['password']):
                    validUser = auth.authenticate(username=dataForm['email'], password=dataForm['password'])
                    auth.login(request, validUser)
                    d = {'status':'success'}
                    return HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signin(data);</script>")
                else:
                    d = {'status':'failed', 'info':'401'}
                    response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signin(data);</script>")
                    response["Error"] = "Invalid Password"
                    return response
            except Exception, err:
                d = {'status':'failed', 'info':'402'}
                response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.signin(data);</script>")
                response["Error"] = "Username Doesn't Exist"
                return response
    return render(request, 'supplier/signin.html', {'form':form})

def signout(request):
    auth.logout(request)
    return HttpResponseRedirect('/supplier/')

@login_required(login_url=loginUrl)
def main(request):
    return render(request, 'supplier/main.html')

@login_required(login_url=loginUrl)
def lokalku(request):
    user = User.objects.get(username=request.user.username)
    userInfo = SupplierInfo.objects.get(sup_email=request.user.username)
    if request.POST:
        if request.POST.has_key('saveProfile'):
            form = EditProfileForm(request.POST)
            if form.is_valid():
                dataForm = form.cleaned_data
                try:
                    userInfo.account.username = dataForm['first_name']
                    userInfo.name = dataForm['last_name']
                    userInfo.address = dataForm['address']
                    userInfo.mobile = dataForm['mobile']
                    userInfo.save()
                    d = {'status':'success'}
                    return HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.updateProfile();</script>")
                except Exception, err:
                    d = {'status':'failed'}
                    response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.updateProfile();</script>")
                    response["Error"] = err
                    return response
        if request.POST.has_key('savePassword'):
            oldPass = request.POST['oldPass']
            newPass = request.POST['newPass']
            if user.check_password(oldPass):
                user.set_password(newPass)
                user.save()
                d = {'status':'success'}
                response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.updatePassword(data);</script>")
                return response
            else:
                d = {'status':'failed'}
                response = HttpResponse("<script type=\"text/javascript\">var data = "+str(d)+"; parent.updatePassword(data);</script>")
                response["Error"] = "Incorrect Password"
                return response
    return render(request, 'supplier/lokalku.html', {'userInfo':userInfo})

@login_required(login_url=loginUrl)
def product(request):
    return render(request, 'supplier/product.html')